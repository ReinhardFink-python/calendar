#!/bin/python

import datetime
import re
import locale
import os

# LaTeX template
import sys

calendar_template_tex = "Kalender_Template.tex"
# finished LaTeX document
calendar_tex = "Kalender.tex"

# marker to separate LaTeX document start -> \begin{document}
start_header = "%%%start_header%%%\n"
end_header = "%%%end_header%%%\n"

# marker to separate LaTeX \begin{document} -> page_1
start_document_header = "%%%start_document_header%%%\n"
end_document_header = "%%%end_document_header%%%\n"

# marker to separate page_1
start_page_1 = "%%%start_page_1%%%\n"
end_page_1 = "%%%end_page_1%%%\n"

# marker to separate page_2
start_page_2 = "%%%start_page_2%%%\n"
end_page_2 = "%%%end_page_2%%%\n"

# marker to separate document end
start_document_end = "%%%start_document_end%%%\n"
end_document_end = "%%%end_document_end%%%\n"

marker_year = "@year"
marker_number_of_school_week = "@numOfSchoolWeek"
marker_number_of_year_week = "@numOfYearWeek"

marker_day_monday = "@mo"
marker_day_tuesday = "@tu"
marker_day_wednesday = "@we"
marker_day_thursday = "@th"
marker_day_friday = "@fr"
marker_day_saturday = "@sa"
marker_day_sunday = "@su"

marker_day_monday_month = "@dmoM"
marker_day_tuesday_month = "@dtuM"
marker_day_wednesday_month = "@dweM"
marker_day_thursday_month = "@dthM"
marker_day_friday_month = "@dfrM"
marker_day_saturday_month = "@dsaM"
marker_day_sunday_month = "@dsuM"


class LaTeXfileCreator:
    def __init__(self, filename, year):
        try:
            file = open(filename, "r")
            self.input = file.readlines()
            file.close()
            self.output = ""
            self.dp = DateProzessor(year)

        except UnicodeDecodeError:
            file.close()

    def read_part(self, start_marker, end_marker):
        string = ""
        inside = False
        for line in self.input:
            if line == start_marker:
                inside = True
            elif line == end_marker:
                string = string + line
                inside = False
            if inside:
                string = string + line
        return string

    def write_header(self):
        print("processing header")
        self.output = self.output + datetime.datetime.now().strftime("%%%- " + "%Y.%m.%d %H:%M:%S" + " -%%%\n")
        header = self.read_part(start_header, end_header)
        header = re.sub(marker_year, self.dp.get_string_year(), header)
        self.output = self.output + header

    def write_document_header(self):
        print("processing document header")
        self.output = self.output + self.read_part(start_document_header, end_document_header)

    def write_page_1(self):
        print("processing page 1 schoolweek: " + self.dp.get_string_numer_of_school_year_week())
        page_1 = self.read_part(start_page_1, end_page_1)
        page_1 = re.sub(marker_number_of_school_week, self.dp.get_string_numer_of_school_year_week(), page_1)
        page_1 = re.sub(marker_year, self.dp.get_string_year(), page_1)
        page_1 = re.sub(marker_number_of_year_week, self.dp.get_string_numer_of_normal_year_week(), page_1)
        self.output = self.output + page_1

    def write_page_2(self):
        print("processing page 2 schoolweek: " + self.dp.get_string_numer_of_school_year_week())
        page_2 = self.read_part(start_page_2, end_page_2)
        page_2 = re.sub(marker_number_of_school_week, self.dp.get_string_numer_of_school_year_week(), page_2)
        page_2 = re.sub(marker_year, self.dp.get_string_year(), page_2)
        page_2 = re.sub(marker_number_of_year_week, self.dp.get_string_numer_of_normal_year_week(), page_2)
        page_2 = re.sub(marker_day_monday, self.dp.get_string_date_day(), page_2)
        page_2 = re.sub(marker_day_monday_month, self.dp.get_string_date_month(), page_2)
        self.dp.next_day()
        page_2 = re.sub(marker_day_tuesday, self.dp.get_string_date_day(), page_2)
        page_2 = re.sub(marker_day_tuesday_month, self.dp.get_string_date_month(), page_2)
        self.dp.next_day()
        page_2 = re.sub(marker_day_wednesday, self.dp.get_string_date_day(), page_2)
        page_2 = re.sub(marker_day_wednesday_month, self.dp.get_string_date_month(), page_2)
        self.dp.next_day()
        page_2 = re.sub(marker_day_thursday, self.dp.get_string_date_day(), page_2)
        page_2 = re.sub(marker_day_thursday_month, self.dp.get_string_date_month(), page_2)
        self.dp.next_day()
        page_2 = re.sub(marker_day_friday, self.dp.get_string_date_day(), page_2)
        page_2 = re.sub(marker_day_friday_month, self.dp.get_string_date_month(), page_2)
        self.dp.next_day()
        page_2 = re.sub(marker_day_saturday, self.dp.get_string_date_day(), page_2)
        page_2 = re.sub(marker_day_saturday_month, self.dp.get_string_date_month(), page_2)
        self.dp.next_day()
        page_2 = re.sub(marker_day_sunday, self.dp.get_string_date_day(), page_2)
        page_2 = re.sub(marker_day_sunday_month, self.dp.get_string_date_month(), page_2)
        self.dp.next_day()
        self.output = self.output + page_2

    def write_document_foot(self):
        print("processing document end")
        self.output = self.output + self.read_part(start_document_end, end_document_end)

    def create_LaTeX_document(self):
        self.write_header()
        self.write_document_header()
        while self.dp.hast_next():
            self.write_page_1()
            self.write_page_2()
        self.write_document_foot()

    def write_tex_file(self, filename):
        try:
            file = open(filename, "w")
            file.write(self.output)
            file.close()
        except UnicodeDecodeError:
            file.close()


class DateProzessor:
    def __init__(self, year):
        self._year = year
        self._number_of_schoolweek = 0
        self._number_of_days = 0
        #  we start 15 days before school starts
        self._current_day = self.get_second_monday_in_september() + datetime.timedelta(days=-15)
        # current day is now -14 days before school starts
        self.next_day()

    def get_string_year(self):
        return self._current_day.strftime("%Y")

    def get_string_numer_of_normal_year_week(self):
        return self._current_day.strftime("%W")

    def get_string_numer_of_school_year_week(self):
        return str(self._number_of_schoolweek)

    def get_string_date_day(self):
        return str(self._current_day.day)

    def get_string_date_month(self):
        return self._current_day.strftime("%B")

    def get_second_monday_in_september(self):
        sep01 = datetime.date(self._year, 9, 1).weekday()
        return datetime.date(self._year, 9, ((7 - sep01) + 1 + 7))

    def get_second_monday_in_july(self):
        july01 = datetime.datetime(self._year + 1, 7, 1).weekday()
        return datetime.date(self._year + 1, 7, ((7 - july01) + 1 + 7))

    def hast_next(self):
        return self._current_day < self.get_second_monday_in_july() + datetime.timedelta(days=28)

    def next_day(self):
        self._current_day = self._current_day + datetime.timedelta(days=1)
        self._number_of_days += 1
        # we start counting with 1 => // 7 + 1
        # but we have week: -2, -1, 1, 2, ...
        self._number_of_schoolweek = (self._number_of_days // 7 + 1) - 3
        if self._number_of_schoolweek >= 0:
            self._number_of_schoolweek += 1
        return self._current_day


# locale.setlocale(locale.LC_ALL, 'de_DE')
locale.setlocale(locale.LC_ALL, '')
if len(sys.argv) < 2:
    print("Bitte Jahreszahl des Wintersemesters eingeben!")
    year = input()
else:
    year = sys.argv[1]
tex_content = LaTeXfileCreator(calendar_template_tex, int(year))
tex_content.create_LaTeX_document()
tex_content.write_tex_file(calendar_tex)

os.system("pdflatex " + calendar_tex)
os.system("rm Kalender.log")
os.system("rm Kalender.aux")
os.system("rm Kalender_Template.log")
os.system("rm Kalender_Template.aux")
os.system("rm Kalender_Template.synctex.gz")
os.system("evince Kalender.pdf")
